/*
* This creates exemples from JSOn schemes.
*
* Use:  $node dirfaker.js C:\input_folder C:\output_folder
*
* prerequisities: node.js, npm, json-schema-faker
* www: https://github.com/json-schema-faker/json-schema-faker
*
* Author: michal.valny@logimic.com
*/

var jsf = require('json-schema-faker');

var fs = require('fs');

function isDirectoryExists(directory) {   
  try {
    fs.statSync(directory);
    return true;
  } catch(e) {
    return false;
  }
}

function listDir(directory) {
  fs.readdir('.', function(err, items) {    
    if (err)    
      throw err;
    for (var index in files) {
      console.log(files[index]);
    }
  }); 
}

var path = require('path');
 
// Return a list of files of the specified fileTypes in the provided dir, 
// with the file path relative to the given dir
// dir: path of the directory you want to search the files for
// fileTypes: array of file types you are search files, ex: ['.txt', '.jpg']
function getFilesFromDir(dir, fileTypes) {
  var filesToReturn = [];
  function walkDir(currentPath) {
    var files = fs.readdirSync(currentPath);
    for (var i in files) {
      var curFile = path.join(currentPath, files[i]);      
      if (fs.statSync(curFile).isFile() && fileTypes.indexOf(path.extname(curFile)) != -1) {
        filesToReturn.push(curFile.replace(dir, ''));
      } else if (fs.statSync(curFile).isDirectory()) {
       walkDir(curFile);
      }
    }
  };
  walkDir(dir);
  return filesToReturn; 
}

// Make example from schema
function resultSchema (pathIn, path) {

  var schema = JSON.parse(fs.readFileSync(pathIn, 'utf8'));

  jsf.resolve(schema).then(function(result) {
     //console.log(JSON.stringify(result, null, 2));
     fs.writeFileSync(path, JSON.stringify(result, null, 2), 'utf8');
     return result;
   }); 
}


jsf.option({
  alwaysFakeOptionals: true,
  useDefaultValue: true
});

var args = process.argv.slice(2);
console.log('Generating faker data from JSON schemes in folder...');
//console.log('Input file:' + process.argv[2]);
//console.log('Output file:' + process.argv[3]);

var dirIn = process.argv[2];
var dirOut = process.argv[3];

var listFiles;

if (!isDirectoryExists(dirIn)) {
  console.log('Input directory does not exist, program quits... :(');
  process.exit(-1);
} else {
  console.log('Input directory: ' + dirIn);

}

if (!isDirectoryExists(dirOut)) {
  console.log('Output directory does not exist, program quits... :(');
  process.exit(-1);
} else {
  console.log('Output directory: ' + dirOut);
}

console.log('Working.................');

// Get list of files...
listFiles = getFilesFromDir(dirIn, [".json"]);

// Go through foles and transfer them...
for (var i of listFiles) {

  var schemaName = path.basename(i, '.json');
  var schemaPath = dirIn + i;

  path.normalize(schemaPath);

  console.log(schemaPath);

  var exampleName = schemaName + '-example.json';
  var examplePath = dirOut + '\\' + exampleName;

  path.normalize(examplePath);

  console.log(examplePath);

  // Create schema example...
  resultSchema(schemaPath, examplePath);
    
}
 
console.log('Done.................:');

    

