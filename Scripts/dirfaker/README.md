# Generate JSON examples from JSON schemas

This JS script generates message examples from all JSON schems in given directory.

## Prerequisities

1. [Node.js](https://nodejs.org)
2. [npm](https://www.npmjs.com/)
3. [Json Schema Faker](https://github.com/json-schema-faker/json-schema-faker)
4. **dirfaker.js** script

## Setup

After download from github open command line in folder where you have **dirfaker.js** and install **json-schema-faker**:

```
$ npm install json-schema-faker --save
```

## Use

Open command line in folder where you have **dirfaker.js**

```
$node dirfaker.js C:\input_folder C:\output_folder
```
