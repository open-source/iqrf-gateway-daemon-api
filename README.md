# Structure

- Documets: Anything related to API documentation (md files, images, uml, ...)
- Ideas: Anything related to API (docs, images, files, e-mails, etc...)
- JsonSchemes: JSON files with schemes and examples.
- Scripts: Useful scripts.

# Requirements

- collected requirements with linked issues [api-requirements.md](Documents/01-Requirements/api-requirements.md)

# Design documents

- collected design document [api-design.md](Documents/02-Design/api-design.md)

# Project status

- We use **Board** for driving API definition project: https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/boards
