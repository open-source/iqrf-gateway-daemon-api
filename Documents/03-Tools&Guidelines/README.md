# Guidelines

## Documents flow

* In **igrf-daemon repo** https://gitlab.iqrfsdk.org/gateway/iqrf-daemon/tree/master/Documents/03-Tools&Guidelines

# Tools

## JSON schema tool

Tool for design and validation of JSON schemes.

1. [json-buddy](http://www.json-buddy.com/)
   - Free 14 days trial
   - $30 for commercial license
   - editor
   - docgen

2. [Atom editor](https://atom.io/)
    - Plugins: ide-json, language-json

3. [Visual Studio Code](https://code.visualstudio.com/)
    - Plugins: JSON Tools, JSON Schema Validator

## Genereate C++ parser from JSON schema

1. http://tomeko.net/software/JSONedit/

## Generate Examples from JSON Schema

1. http://json-schema-faker.js.org

## Generate Documentation from JSON schema

1. https://github.com/mattyod/matic/
