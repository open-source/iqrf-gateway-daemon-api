# JSON RPC

http://www.jsonrpc.org/specification

It is designed to be simple!

## 1 Overview

JSON-RPC is a stateless, light-weight remote procedure call (RPC) protocol. Primarily this specification defines several data structures and the rules around their processing. It is transport agnostic in that the concepts can be used within the same process, over sockets, over http, or in many various message passing environments. It uses JSON (RFC 4627) as data format.

* Transport agnostic
    * MQ, MQTT, HTTP, ...

* Language agnostic
    * C++, Python, PHP, Javascript, ...

## 2 IQRF API

### Comm

--> {"jsonrpc": "2.0", "method": "comm_SendDpaRaw", "params": {"[timeout]":<INT>, "request":"<STRING>", ... }, "id": 1}
<-- {"jsonrpc": "2.0", "result": {"[timeout]":<INT>, "request":"<STRING>", ...}, "id": 1}

--> {"jsonrpc": "2.0", "method": "comm_SendDpaRawHdp", "params": {"[timeout]":<INT>, "nadr":"<STRING>", "pnum":"<STRING>", ... }, "id": 2}
<-- {"jsonrpc": "2.0", "result": {"[timeout]":<INT>, "nadr":"<STRING>", "pnum":"<STRING>", ... }, "id": 2}

...

### Service

--> {"jsonrpc": "2.0", "method": "service_BondNode", "params": {"[timeout]":<INT>, "nadr":"<STRING>", ... }, "id": 3}
<-- {"jsonrpc": "2.0", "result": <BOOLEAN>, "id": 3}

...

### Config

--> {"jsonrpc": "2.0", "method": "config_ChangeMode", "params": {"mode":"<STRING>", ... }, "id": 4}
<-- {"jsonrpc": "2.0", "result": <BOOLEAN>, "id": 4}

...

### Stats

--> {"jsonrpc": "2.0", "method": "stats_FromNode", "params": {"nadr":"<STRING>", ... }, "id": 5}
<-- {"jsonrpc": "2.0", "result": {...}, "id": 5}

...

## MQTT channel

### MQTT basic

MQTT is a lightweight publish-subscribe messaging protocol which probably makes it the most suitable for various IoT devices. 
You can find more information about MQTT here.

### API

Topics definition:

api/v2/iqrf/comm/+
api/v2/iqrf/service/+
api/v2/iqrf/config/+
api/v2/iqrf/stats/+

### Example

#### Request

mosquitto_pub -d -h "127.0.0.1" -t "api/v2/iqrf/comm/request" {"jsonrpc": "2.0", "method": "comm_SendDpaRaw", "params": {"[timeout]":<INT>, "request":"<STRING>", ... }, "id": 1}

#### Response

mosquitto_sub -d -h "127.0.0.1" -t "api/v2/iqrf/comm/response"

{"jsonrpc": "2.0", "result": {"[timeout]":<INT>, "request":"<STRING>", ...}, "id": 1}

## HTTP channel

### HTTP basics

HTTP is a general-purpose network protocol that can be used in IoT applications. You can find more information 
about HTTP here. HTTP protocol is TCP based and uses request-response model.

IQRF daemon acts as an HTTP Server that supports both HTTP and HTTPS protocols

### API

URL definition:

http(s)://host:port/api/v2/iqrf/comm
http(s)://host:port/api/v2/iqrf/service
http(s)://host:port/api/v2/iqrf/config
http(s)://host:port/api/v2/iqrf/stats

### Example

#### Request
curl -v -X POST -d {"jsonrpc": "2.0", "method": "comm_SendDpaRaw", "params": {"[timeout]":<INT>, "request":"<STRING>", ... }, "id": 1} http://localhost:8080/api/v2/iqrf/comm --header "Content-Type:application/json"

#### Response
{"jsonrpc": "2.0", "result": {"[timeout]":<INT>, "request":"<STRING>", ...}, "id": 1}

## Feedback

https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/2
