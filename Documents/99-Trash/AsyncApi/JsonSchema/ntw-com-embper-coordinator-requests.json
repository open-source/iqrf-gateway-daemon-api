{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"$id": "http://example.com/example.json",
	"definitions": {
		"params_addr_info": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		},
		"params_discovered_devices": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		},
		"params_bonded_devices": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		},
		"params_clear_all_bonds": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		},
		"params_bond_node": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"req_addr": {
					"type": "integer",
					"description": "A requested address for the bonded node. The address must not be used (bonded) yet. If this parameter equals to 0, then the 1st free address is assigned to the node.",
					"default": 0
				},
				"bonding_mask": {
					"type": "integer",
					"description": "See IQRF OS User's and Reference guides (remote bonding, function bondNewNode)."
				}
			}
		},
		"params_remove_bond": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"bond_addr": {
					"type": "integer",
					"description": "Address of the node to remove the bond to."
				}
			}
		},
		"params_rebond_node": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"bond_addr": {
					"type": "integer",
					"description": "Address of the node to be re-bonded."
				}
			}
		},
		"params_discovery": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"tx_power": {
					"type": "integer",
					"description": "TX Power used for discovery."
				},
				"max_addr": {
					"type": "integer",
					"description": "Nonzero value specifies maxItems node address to be part of the discovery process."
				}
			}
		},
		"params_set_dpaparam": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"dpa_value": {
					"type": "string",
					"description": "Configuration of the type of DPA value.",
					"enum": [
						"LastRssi",
						"Voltage",
						"System",
						"User"
					]
				},
				"led_diagnostic": {
					"type": "boolean",
					"description": "Diagnosing the network behavior based on LED activities. Red LED flashes when Node or Coordinator receives network message. Green LED flashes when Coordinator sends network message or when Node routes network message."
				},
				"fixed_timeslot": {
					"type": "boolean",
					"description": "A long fixed 200 ms timeslot is used. It allows easier tracking of network behavior."
				}
			}
		},
		"params_set_hops": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"request_hops": {
					"type": "integer",
					"description": "Allows the specifying fixed number of hops used to send the DPA request or to specify an optimization algorithm to compute a number of hops.",
					"default": 255
				},
				"response_hops": {
					"type": "integer",
					"description": "Allows the specifying fixed number of hops used to send the DPA response or to specify an optimization algorithm to compute a number of hops.",
					"default": 255
				}
			}
		},
		"params_discovery_data": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"address": {
					"type": "integer",
					"description": "Address of the discovery data to read. Allows reading of coordinator internal discovery data."
				}
			}
		},
		"params_backup": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"index": {
					"type": "integer",
					"description": "This command reads coordinator network information data that can be then restored to another coordinator in order to make a clone of the original coordinator."
				}
			}
		},
		"params_restore": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"network_data": {
					"type": "array",
					"items": {
						"type": "integer"
					},
					"maxItems": 48,
					"description": "One block of the coordinator network info data previously obtained by Backup command."
				}
			}
		},
		"params_authorize_bond": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"req_addr": {
					"type": "integer",
					"description": "See https://www.iqrf.org/DpaTechGuide/#3.2.15%20Authorize%20bond request. If 0xFF is specified then the pre-bonded node is unbonded and then reset."
				},
				"mid": {
					"type": "array",
					"items": {
						"type": "integer"
					},
					"maxItems": 3,
					"description": "Module ID of the node to be authorized."
				}
			}
		},
		"params_enable_bonding": {
			"type": "object",
			"description": "Parameters for given command.",
			"properties": {
				"bonding_mask": {
					"type": "integer",
					"description": "See IQRF OS User's and Reference guides (remote bonding, function bondNewNode)."
				},
				"control": {
					"type": "boolean",
					"description": "Enables remote bonding mode. If enabled then previously bonded nodes are forgotten."
				},
				"user_data": {
					"type": "array",
					"items": {
						"type": "integer"
					},
					"maxItems": 3,
					"description": "Optional data that can be used at Reset Custom DPA Handler event."
				}
			}
		},
		"params_read_remotely_bonded_mid": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		},
		"params_clear_remotely_bonded_mid": {
			"type": "object",
			"description": "No parameters.",
			"properties": {}
		}
	},
	"type": "object",
	"properties": {
		"mcat": {
			"type": "string",
			"description": "Message category.",
			"enum": [
				"ntw-com-embper-coordinator"
			]
		},
		"msg": {
			"type": "object",
			"properties": {
				"msgid": {
					"type": "string",
					"description": "Message identification for binding request with response."
				},
				"timeout": {
					"type": "integer",
					"description": "Timeout to wait for IQRF DPA response."
				},
				"req": {
					"type": "object",
					"properties": {
						"nadr": {
							"type": "string",
							"description": "Network device address."
						},
						"hwpid": {
							"type": "string",
							"description": "Hardware profile identification."
						},
						"cmd_str": {
							"type": "string",
							"description": "Peripheral command in a string form.",
							"enum": [
								"AddrInfo",
								"DiscoveredDevices",
								"BondedDevices",
								"ClearAllBonds",
								"BondNode",
								"RemoveBond",
								"RebondNode",
								"Discovery",
								"​SetDpaParam",
								"​SetHops",
								"DiscoveryData",
								"​Backup",
								"Restore",
								"​AuthorizeBond",
								"​EnableRemoteBonding",
								"ReadRemotelyBondedMid",
								"ClearRemotelyBondedMid"
							]
						},
						"params": {
							"anyOf": [
								{
									"$ref": "#/definitions/params_addr_info"
								},
								{
									"$ref": "#/definitions/params_discovered_devices"
								},
								{
									"$ref": "#/definitions/params_bonded_devices"
								},
								{
									"$ref": "#/definitions/params_clear_all_bonds"
								},
								{
									"$ref": "#/definitions/params_bond_node"
								},
								{
									"$ref": "#/definitions/params_remove_bond"
								},
								{
									"$ref": "#/definitions/params_rebond_node"
								},
								{
									"$ref": "#/definitions/params_discovery"
								},
								{
									"$ref": "#/definitions/params_set_dpaparam"
								},
								{
									"$ref": "#/definitions/params_set_hops"
								},
								{
									"$ref": "#/definitions/params_discovery_data"
								},
								{
									"$ref": "#/definitions/params_backup"
								},
								{
									"$ref": "#/definitions/params_restore"
								},
								{
									"$ref": "#/definitions/params_authorize_bond"
								},
								{
									"$ref": "#/definitions/params_enable_bonding"
								},
								{
									"$ref": "#/definitions/params_read_remotely_bonded_mid"
								},
								{
									"$ref": "#/definitions/params_clear_remotely_bonded_mid"
								}
							]
						}
					},
					"required": [
						"nadr",
						"cmd_str"
					]
				},
				"return_verbose": {
					"type": "boolean",
					"description": "Flag that includes additional parameters in the response."
				}
			},
			"required": [
				"msgid",
				"req"
			]
		}
	},
	"required": [
		"mcat",
		"msg"
	]
}
