# Async API

Message oriented communication between IQRF Daemon GW and clients.

Messages can be sent from both sides (asynchronously).

Client originated messages are responded by GW.

GW originated messages are not expected to be responded.

Used for rather small payload e.g. transfer of data from sensors

Messages payload encoded in JSON http://www.json.org/ 

## Topics

Messages are distinguished according Topic in form of string:

**iqrf.daemongw.{type}.{version}.{action}**

## DPA messages

Hold DPA request/response

Topic:
**iqrf.daemongw.dpa.1.{action}**


### DpaRequest

Client -> GW
- DpaRequest to provide DPA command in GW

Topic:
**iqrf.daemongw.dpa.1.request**

Payload:
[DpaRequest_sch.json](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/blob/master/JsonSchema/DpaRequest_sch.json)

TODO
*In some special cases an async DPA request can emerge within IQRF network and GW can originate DPA request ?*

### DpaResponse

GW -> Client
- DpaResponse is sent as a reply to DpaRequest sent by Client
- DpaResponse as a result of scheduled action
- DpaResponse as a result of DPA asynchronous message 

Topic:
**iqrf.daemongw.dpa.1.response**

Payload:
[DpaResponse_sch.json](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/blob/master/JsonSchema/DpaResponse_sch.json)

## Development Tools

### JSON Schema

Formal description of JSON formats. It can be used by existing tools:
- editor
- documentation generator
- validator
- code generator

#### Specification:
http://json-schema.org/
http://json-schema.org/specification.html

Excelent guide:
https://spacetelescope.github.io/understanding-json-schema/

#### Tools:
- [json-buddy](http://www.json-buddy.com/)
 - Free 14 days trial
 - $30 for commercial license
 - editor
 - docgen
- [Altova](https://www.altova.com/download-xml-editor-b?gclid=EAIaIQobChMIjPPu5vjF2AIVrbvtCh06WQAJEAAYAiAAEgLXWPD_BwE)
 - Free 30 days trial
 - 391,00 EUR for commercial perpetual license / user
 - editor (many formats XML, JSON ...)
 - docgen (many design styles)
 - profi solution, but quite expensive for few documents ...
- [quicktype](https://app.quicktype.io) exists as [VSCode plugin](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)
 -  editor
 -  online
- [json-schema-editor](https://json-schema-editor.tangramjs.com/)
 - editor

These were not tested yet
- [lbovet](http://lbovet.github.io/docson/index.html#/docson/examples/example.json)
- [cloudflare](https://blog.cloudflare.com/cloudflares-json-powered-documentation-generator/)
- [npmjs](https://www.npmjs.com/package/json-schema-docs-generator)

TODO evaluate
- validators libraries (C++)
- code generators (C++)
