# API Requirements [#17](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/17)

#### **API-00** Select API support tools [#1](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/1)

Select support tools for:
 - messages desig, schema
 - code generators for both server, client side
 - validators (based on schema)
 - automated documentation

#### **API-01** Async API [#15](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/15)

- Asynchronous message driven communication with IQRF GW Daemon
- Designed to comunicate via:
  - MQ channel (inter process)
  - MQTT channel (broker, clouds)
  - Websockets (standard inter process, network, dockerizable)

#### **API-02** IQRF Network Basic DPA messages [#5](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/5)
- Messages via Async API
- Performing DPA requests and getting back DPA responses
- Requests/Responses:
 1. raw
 2. raw-hdp

Json structure with ctype and type will be also implemented to keep compatibility with v1. 

#### **API-03** IQRF Network Embedded Peripheral messages
- Messages via Async API
- Uses IQRF Repository Javascript drivers to encode/decode DPA requests/responses
- Requests/Responses depends on (IQRF Respository):
 1. embed led
 2. embed thermometer
 3. embed io
 4. embed frc
 5. embed os
 ... 

All embed types are available [here](https://repository.iqrfalliance.org/doc/drivers/).
It should be easy to add new types as they appear in the future.

#### **API-04** IQRF Network Standard Device messages [#12](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/12)
- Messages via Async API
- Uses IQRF Repository Javascript drivers to encode/decode DPA requests/responses
- Requests/Responses depends on (IQRF Respository):
 1. standard device enum
 2. standard sensor
 3. standard binary output
 4. standard light

All standard types are available [here](https://repository.iqrfalliance.org/doc/drivers/).
It should be easy to add new types as they appear in the future.

#### **API-05** IQRF Network Management messages [#13](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/13)
- Messages via Async API
- Allow to user (integrator) to work with IQRF network without deep knowledge of DPA and IDE.
- Request/Responses:
 1. Configure (RF channel, Access Password, ...)
 2. Local bonding
 3. Network test (Enumerace) - table, info about errors, suggestion for updates
 4. TR backup
 5. TR restore
 6. TR upload
 7. TR configuration
 8. Schedule task
 9. OTA upload plugin
 10. OTA upload handler
 11. OS+DPA update
 12. Node statistics (node address or aggregation of all)
     - number of packets successfuly received (node - > coord)
     - number of packets successfuly transmited (coord - > node)
     - number of bytes successfuly transmited (node - > coord)
     - number of bytes successfuly transmited (coord -> node)
     - number of packets timeouted (coord -> node)
     - RSSI from the last communication node directly communicating with coordinator (can also be the same node)
     - timestamp of the last successfuly received packet (coord -> node)
     - timestamp of the last successfuly transmited packet (node -> coord)
     - timestamp of the last timouted packet (node -> coord)

Details will be worked out gradually. Changes may be introduced.

#### **API-06** IQRF GW Daemon Management messages [#9](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/9)
- Messages via Async API
- Performing configuration request
- Request/Responses:
 1. Daemon operation mode selection (operational/service/forwarding)
 2. Daemon restart
 3. Daemon components configuration

#### **API-07** IQRF GW Daemon Scheduler messages [#7](https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/issues/7)
- Messages via Async API
- Command IQRF GW Daemon internal scheduler to perform tasks understandable by running service as the task consumer.
- Request/Responses:
 1. Add task periodic (in seconds) for service id
 2. Add task cron syntax for service id
 3. Get task by id
 4. Remove task by id
 5. List all tasks for service id
 6. Remove all tasks for service id

#### **API-08** General messages format requirements
- Messages have JSON format
- Extended format (coded as JSON string)
 - BYTE (haxadecimal) in lowercase e.g: `1a`
 - WORD (haxadecimal) in lowercase e.g: `1a2b` 
 - BINARY (haxadecimal) in lowercase, bytes separated by dot e.g: `02.00.4b.3e.ff.ff`
 - TIMESTAMP follow iso8601 e.g: `2018-01-03T16:00:09.859515`
- Item names are lowercase, words split by underscore, e.g: **"response_ts"**
- String values are arbitrary
- Not applicable items are omitted from response completely (rather then empty string) e.g. raw-hdp response omits `rdata` if the value is not present in received DPA response

#### **API-09** Messages Schemes
- Messages have to be described by JSON Schema
- Schemes shall be part of design phase
- Schemes are used for detailed documentation generation by appropriate tool
- Schemes may be used by automatic validators both on GW and Client side
