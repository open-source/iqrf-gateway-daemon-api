# IQRF GW IQMESH Communication API

## JSON schema definition

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
                "ntw-com",
                "dpa (for compatibility with v1)"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "raw",
                "raw-hdp",
                "emb-per",
                "std-dev"
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        }
    }
}
```

# IQRF GW IQMESH Management API

## JSON schema definition

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
                "ntw-mng"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "service-task",
                "service-cfg"
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        },
        "service":
		{
            "description": "IQMESH service name",
            "type": "string",
            "enum": [
                "tr-setting",
                "local-bonding",
                "enumeration"
			]
        }
    }
}
```

# IQRF GW Daemon Management API

## JSON schema definition

### All types

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
				"daemon-mng"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "runtime-cfg",
                "component-cfg",
                "service-cfg"
			]
        }
    }
}
```

### Runtime configuration

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
				"daemon-mng"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "runtime-cfg"
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        },
        "parameters": {
            "type": "object",
            "properties": {
                "mode": {"type": "string"}
            }
        }
    }
}
```

### Component configuration

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
				"daemon-mng"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "component-cfg",
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        },
        "component": 
        {
            "type": "string",
            "enum": [
                "config_tracer_file"
			]
        },
        "properties": {
            "type": "object",
            "properties": {
                "trace_file_name": {"type": "string"},
                "trace_file_size": {"type": "string"},
                "verbosity_level": {"type": "string"}
            }
        }
    }
}
```

### Service configuration

```json
{
	"definitions":
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
				"daemon-mng"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "service-cfg",
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        },
        "service":
        {
            "type": "string",
            "enum": [
                "scheduler"
			]
        },
        "properties": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "id": {"type": "string"},
                "time_pattern": {"type": "string"},
                "task": {"type": { "enum": [ "string", "json" ]}}
            }
        }
    }
}
```

# IQRF GW Daemon Statistics API

```json
{
	"definitions": 
	{
		"ctype":
		{
            "description": "Category type",
			"type": "string",
            "enum": [
				"daemon-stats"
			]
        },
        "type":
		{
            "description": "Message types of a given category",
			"type": "string",
            "enum": [
                "node-pkts",
                "network-pkts",
                "topology"
			]
        },
        "msg_id":
		{
            "description": "Message identification",
			"type": "string"
        }
    }
}
```
