# IQMESH network management

## Service: Local bonding

Služba lokálně přibonduje Noda a úspěšnost ověří pingem.

## Funkcionalita služby:

- Zjistí se, zda je požadovaná adresa volná. Pokud ne vrátí: ERROR_ADDRESS_ALREADY_BONDED / ERROR_ALL_ADDRESS_OCCUPIED
- Provede se DPA request Bond Node (https://www.iqrf.org/DpaTechGuide/#3.2.6%20Bond%20node)
- Při neúspěchu se vrátí: ERROR_BONDING_FAILED
- Při úspěchu se provede (opakovaně *) DPA request OS Read (https://www.iqrf.org/DpaTechGuide/#3.4.2%20Read)
- Pokud nepřijde response, Node se na straně [C] odbonduje a vrátí se: ERROR_BONDING_FAILED
- Pokud ano, vrátí se: OK

* Počet opakování pevný/konfigurovatelný? Pokud konfigurovatelný, tak přes konfiguraci nebo v každém requestu jako nepovinný parametr např:
"[repeat]": "<INT>"

Request
{
    "ctype": "dpa",
    "type": "ntw_mng",
	"msgid": "<STRING>",   // ??? 
    "service": "BOND_NODE_LOCAL",
	"params": {
            "requested_addr": "<INT>"
    }
}

Response
{
    "ctype": "dpa",
    "type": "ntw_mng",
	"msgid": "<STRING>",   // ???
    "service": "BOND_NODE_LOCAL",
	"results": {
            "assigned_address": "<INT>",
            "nodes": "<INT>",
			"hwpid": "<WORD>",
            "manufacturer": "<STRING>",
			"product": "<STRING>",
			"iqrf_os_version": "<STRING>",  // ???
			"tr_module_id": "<STRING>"
    },
	"status": "<STRING>"
}

Poznámka:
Do JSON struktur výše bude jěště nutné zapracovat některé prvky i jako "čísla" (Hynkova připomínka) pro snadnější práci. 
Např. HWPID by tam mělo být i jako <WORD>. Podobně i OS verze atd.

Status codes:
OK
ERROR_BONDING_FAILED   // nedopadne bondování nebo ping
ERROR_ADDRESS_ALREADY_BONDED
ERROR_ALL_ADDRESS_OCCUPIED


Example
-------
Request
{
    "ctype": "dpa",
    "type": "ntw_mng",
	"msgid": "<STRING>",   // ??? 
    "service": "BOND_NODE_LOCAL",
	"params": {
            "requested_addr": "5"
    }
}

Response
{
    "ctype": "dpa",
    "type": "ntw_mng",
	"msgid": "<STRING>",   // ???
    "service": "BOND_NODE_LOCAL",
	"results": {
            "assigned_address": "5",
            "nodes": "101",
            "hwpid":
			"manufacturer": "IQRF Tech s.r.o.",
			"product": "TR temperature sensor example",
			"iqrf_os_version": "4.02D (08B8)",  // ???
			"tr_module_id": "830001C6"
    },
	"status": "OK"
}

Dotazy:

1. Jak jsou/budou řešeny parametry mimo rozsah povolených hodnot? Např. requested_addr = 255
2. Timeout???
3. Časové značky ts
4. Params, Results ... v1 je plochá
5. Služba běží, máš smůlu ... hned STATUS=BUSY, ověřit s Frantou

===============================================================================

## Services:

1. Get/Set RF channel - band // ???

## Funkcionalita služby:
- ověří se platnost vstupních parametrů
- nastaví kanál v rámci koord-nodu-sítě // ???
- Volání DPA requestu https://www.iqrf.org/DpaTechGuide/#3.4.7%20Write%20HWP%20configuration%20byte
- ověří se správné nastavení kanálu pingem


2. Get/Set Access password

## Funkcionalita služby:
- ověří se platnost vstupních parametrů
- provede se DPA request Set Security https://www.iqrf.org/DpaTechGuide/#3.4.10%20Set%20Security


3. Make local bonding

## Funkcionalita služby:

- Zjistí se, zda je požadovaná adresa volná. Pokud ne vrátí: ERROR_ADDRESS_ALREADY_BONDED / ERROR_ALL_ADDRESS_OCCUPIED
- Provede se DPA request Bond Node (https://www.iqrf.org/DpaTechGuide/#3.2.6%20Bond%20node)
- Při neúspěchu se vrátí: ERROR_BONDING_FAILED
- Při úspěchu se provede (opakovaně **) DPA request OS Read (https://www.iqrf.org/DpaTechGuide/#3.4.2%20Read)
- Pokud nepřijde response, Node se na straně [C] odbonduje a vrátí se: ERROR_BONDING_FAILED
- Pokud ano, vrátí se: OK

** Počet opakování pevný/konfigurovatelný? Pokud konfigurovatelný, tak přes konfiguraci nebo v každém requestu jako nepovinný parametr např:
"[repeat]": "<INT>"

4. Make discovery

## Funkcionalita služby:



5. Make enumeration

## Funkcionalita služby:

- začít s min. verzí
- table, info about errors, suggestion for updates, saving the table


6. Do TR backup

## Funkcionalita služby:
option to save/export


7. Do TR restore

## Funkcionalita služby:
option to save/export


8. Do TR upload 

## Funkcionalita služby:


9. Do TR configuration

## Funkcionalita služby:


10. Schedule task

## Funkcionalita služby:

