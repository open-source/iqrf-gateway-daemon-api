# N1 embedded ledr PULSE

{
    "ctype":"dpa",
    "type":"emb_ledr",
    "msgid":"1",
    "nadr":"1",
    "profile_id":"ffff",
    "timeout":1000,
    "request":"pulse",
    "request_ts":"",
    "response_ts":""
}

# N2 embedded ledg ON

{
    "ctype":"dpa",
    "type":"emb_ledg",
    "msgid":"1",
    "nadr":"2",
    "profile_id":"ffff",
    "timeout":1000,
    "request":"on",
    "request_ts":"",
    "response_ts":""
}
