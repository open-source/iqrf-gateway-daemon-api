# N1 LEDR Pulse
http://iqrf.org/products/transceivers/tr-72d

{
    "ctype":"dpa",
    "type":"raw-hdp",
    "msgid":"1",
    "timeout":1000,
    "nadr":"01",
    "pnum":"06",
    "pcmd":"03",
    "hwpid":"ffff",
    "rdata":""
}

{
    "ctype": "dpa",
    "type": "raw-hdp",
    "msgid": "1",
    "timeout": 1000,
    "nadr": "01",
    "pnum": "06",
    "pcmd": "83",
    "hwpid": "0002",
    "rcode": "00",
    "dpaval": "62",
    "rdata":"",
    "status": "STATUS_NO_ERROR"
}

## Optional: with raw structure

{
    "ctype":"dpa",
    "type":"raw-hdp",
    "msgid":"1",
    "timeout":1000,
    "nadr":"01",
    "pnum":"06",
    "pcmd":"03",
    "hwpid":"ffff",
    "rdata":"",
    "request":"",
    "request_ts":"",
    "confirmation":"",
    "confirmation_ts":"",
    "response":"",
    "response_ts":""
}

{
    "ctype": "dpa",
    "type": "raw-hdp",
    "msgid": "1",
    "timeout": 1000,
    "nadr": "01",
    "pnum": "06",
    "pcmd": "83",
    "hwpid": "0002",
    "rcode": "00",
    "dpaval": "62",
    "rdata":"",
    "request": "01.00.06.03.ff.ff",
    "request_ts": "2018-01-02T16:30:41.602048",
    "confirmation": "01.00.06.03.ff.ff.ff.63.01.04.01",
    "confirmation_ts": "2018-01-02T16:30:41.658419",
    "response": "01.00.06.83.02.00.00.62",
    "response_ts": "2018-01-02T16:30:41.781896",
    "status": "STATUS_NO_ERROR"
}
