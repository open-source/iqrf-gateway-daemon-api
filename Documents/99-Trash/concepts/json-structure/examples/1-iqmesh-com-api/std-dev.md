# N1 standard sensor DDC-SE

{
    "ctype":"dpa",
    "type":"std_sen",
    "msgid":"1",
    "nadr":"1",
    "profile_id":"ffff",
    "timeout":1000,
    "request":"enum",
    "request_ts":"",
    "response_ts":""
}

# N2 standard binary output DDC-RE

{
    "ctype":"dpa",
    "type":"std_bout",
    "msgid":"1",
    "nadr":"2",
    "profile_id":"ffff",
    "timeout":1000,
    "request":"enum",
    "request_ts":"",
    "response_ts":""
}
