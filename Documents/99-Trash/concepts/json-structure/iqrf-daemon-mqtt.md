# MQTT Topic definition

Access control and permission of topics.
https://docs.loraserver.io/install/mqtt-auth/

This concept is applicable only to MQTT channel, leaving MQ and WS aside.

## Basic level without any filtering (compatibility v1)

IQRFD Sub: "iqrf/requests"
IQRFD Pub: "iqrf/responses"

## Advanced level with filtering (enable to set user rights per ctype, type)

IQRFD Sub: "iqrf/{ctype}/{type}/requests"
IQRFD Pub: "iqrf/{ctype}/{type}/responses"

## Configuration

Let the user to select her/his choice in the config file.
Default value is Basic.
