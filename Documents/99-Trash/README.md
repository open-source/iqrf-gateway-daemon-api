# Structure

- ideas : for anything largely creative
- concepts: for more detailed specifications and drafts
- roadmap : to define the main steps without immediately looking at all the details
- design : basically, everything that is not text

# Requirements

- collected requirements with linked issues [api-reqs.md](concepts/docs/api-reqs/api-reqs.md)

# Design documents

- collected design document [api-design.md](concepts/docs/api-design/api-design.md)

# Project status

- We use **Board** for driving API definition project: https://gitlab.iqrfsdk.org/gateway/iqrf-daemon-api/boards
