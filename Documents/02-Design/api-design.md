# API Design

## 1 MQTT Topics

IQRF Daemon suffix is:
```
iqrf/mcat
```
* _iqrf_ - constant identifier
* _mcat_ - IQRF message category (unique command identifier)

### Azure examples [(link..)](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-mqtt-support#using-the-mqtt-protocol-directly)

* gateway-to-cloud messages

```
devices/{device_id}/messages/events/iqrf/mcat
```

* cloud-to-gateway messages

```
devices/{device_id}/messages/devicebound/iqrf/mcat
```

### IBM Cloud examples [link..](https://console.bluemix.net/docs/services/IoT/gateways/mqtt.html#mqtt)

* gateway-to-cloud messages

```
iot-2/type/mygateway/id/gateway1/evt/status/fmt/json/iqrf/mcat
```

* cloud-to-gateway messages

```
iot-2/type/typeId/id/deviceId/cmd/commandId/fmt/formatString/iqrf/mcat
```

## 2 Messages

### 2.1 IQRF Network Basic DPA messages

### 2.1.1 Raw DPA

* **Message category (mcat):** ntw-com-raw
* **Request:** Sender-to-gateway request to obtain Raw DPA Response. Message contains unique identifier which will be returned in response, timeout for IQRF network, request in form of hexa-decimal number coded into string e.g. _00.00.06.03.ff.ff_ and verbosity parameter which requests to get response in simple or extended form. Timeout and verbosity parameters are optional.
* **Response:** Gateway-to-sender response as a reaction to request. Message contains unique identifier from request, timeout for IQRF network, response in form of hexa-decimal number coded into string e.g. _00.00.06.03.ff.ff_, raw data, status of response and status of response in string format. Timeout, raw data and status string are optional. Raw data are.
* Schemas:   
  * [ntw-com-raw-request.json](../../JsonSchemes/async-api-json-schemes/ntw-com-raw-request.json),
  * [ntw-com-raw-response.json](../../JsonSchemes/async-api-json-schemes/ntw-com-raw-response.json)
* Example: [request](../../JsonSchemes/async-api-json-schemes/examples/msg-ntw-com-raw-request.json), [response](../../JsonSchemes/async-api-json-schemes/examples/msg-ntw-com-raw-response.json)

### 2.1.2 Raw HDP

* **Message category (mcat):** ntw-com-raw-hdp
* TODO: Logic
* **Schemas:**
  * [ntw-com-raw-hdp-request.json](../../JsonSchemes/async-api-json-schemes/ntw-com-raw-hdp-request.json)
  * [ntw-com-raw-hdp-response.json](../../JsonSchemes/async-api-json-schemes/ntw-com-raw-hdp-response.json)
* **Examples:** [request](../../JsonSchemes/async-api-json-schemes/examples/msg-ntw-com-raw-hdp-request.json), [response](../../JsonSchemes/async-api-json-schemes/examples/msg-ntw-com-raw-hdp-response.json)

### 2.2 IQRF Network Embedded Peripheral messages

### 2.2.1 Embedded Periphery Thermometer
#### 2.2.1.1 Read Temperature

* **Message category (mcat):** comEperThermometerRead
* Reads temperature from embedded sensor. Request parameters are nadr and hwpid. Response returns nadr, hwpid, rcode, dpaval, and temperature in Celsius.
* Schemas:
  * [comEmbperThermometer-request.json](../../JsonSchemes/async-api-json-schemes/comEperThermometerRead-request.json)
  * [comEmbperThermometer-response.json](../../JsonSchemes/async-api-json-schemes/comEperThermometerRead-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperThermometerRead-request-example.json),  [response](../../JsonSchemes/async-api-json-schemes/examples/comEperThermometerRead-response-example.json)

### 2.2.2 Embedded Periphery LED (ledg & ledr)

#### 2.2.2.1 Set On/Off

* **Message category (mcat):** comEperLedgSet, comEperLedrSet
* Turns On/Off green or red LED.
* Schemas:
  * [comEperLedgSet-request.json](../../JsonSchemes/async-api-json-schemes/comEperLedgSet-request.json)
  * [comEperLedgSet-response.json](../../JsonSchemes/async-api-json-schemes/comEperLedgSet-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperLedgSet-request-example.json),  [response](../../JsonSchemes/async-api-json-schemes/examples/comEperLedgSet-ledg-response-example.json)

#### 2.2.2.2 Get Status (On/Off)

* **Message category (mcat):** comEperLedgGet, comEperLedrGet
* Get status On/Off of green or red LED.
* Schemas:
  * [comEperLedgGet-request.json](../../JsonSchemes/async-api-json-schemes/comEperLedgGet-request.json)
  * [comEperLedgGet-response.json](../../JsonSchemes/async-api-json-schemes/comEperLedgGet-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperLedgGet-request-example.json),  [response](../../JsonSchemes/async-api-json-schemes/examplescomEperLedgGet-response-example.json)

#### 2.2.2.3 Pulse request On/Off

* **Message category (mcat):** comEperLedgPulse, comEperLedrPulse
* Turns pulse On/Off of green or red LED.
* Schemas:
  * [comEperLedgPulse-request.json](../../JsonSchemes/async-api-json-schemes/comEperLedgPulse-request.json)
  * [comEperLedgPulse-response.json](../../JsonSchemes/async-api-json-schemes/comEperLedgPulse-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperLedgPulse-request-example.json),  [response](../../JsonSchemes/async-api-json-schemes/examples/comEperLedgPulse-response-example.json)

### 2.2.3 Embedded Periphery Coordinator

#### 2.2.3.1 Get Address

* **Message category (mcat):** comEperCoordGetAddr
* Return coordinator address.
* Schemas:
  * [comEperCoordGetAddr-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordGetAddr-request.json)
  * [comEperCoordGetAddr-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordGetAddr-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.2 Get Bonded Devices

* **Message category (mcat):** comEperCoordGetBonds
* Returns a bitmap of bonded nodes.
* Schemas:
  * [comEperCoordGetBonds-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordGetBonds-request.json)
  * [comEperCoordGetBonds-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordGetBonds-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.3 Clear All Bonds

* **Message category (mcat):** comEperCoordClearBonds
* Removes all nodes from the list of bonded nodes at coordinator memory.
* Schemas:
  * [comEperCoordClearBonds-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordClearBonds-request.json)
  * [comEperCoordClearBonds-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordClearBonds-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.4 Bond Node

* **Message category (mcat):** comEperCoordBondNode
* Bonds a new node by the coordinator.
* Schemas:
  * [comEperCoordBondNode-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordBondNode-request.json)
  * [comEperCoordBondNode-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordBondNode-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.5 Remove Bond

* **Message category (mcat):** comEperCoordRemoveBond
* Removes already bonded node from the list of bonded nodes at coordinator memory.
* Schemas:
  * [comEperCoordRemoveBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordRemoveBond-request.json)
  * [comEperCoordRemoveBond-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordRemoveBond-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.6 Re-bond Node

* **Message category (mcat):** comEperCoordReBond
* Puts specified node back to the list of bonded nodes in the coordinator memory.
* Schemas:
  * [comEperCoordReBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordReBond-request.json)
  * [comEperCoordReBond-respons.json](../../JsonSchemes/async-api-json-schemes/comEperCoordReBond-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.7 Discovery

* **Message category (mcat):** comEperCoordDiscovery
* Runs IQMESH discovery process.
* Schemas:
  * [comEperCoordDiscovery-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordDiscovery-request.json)
  * [comEperCoordDiscovery-respons.json](../../JsonSchemes/async-api-json-schemes/comEperCoordDiscovery-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.8 Set DPA Params

* **Message category (mcat):** comEperCoordSetDpaParams
* Sets DPA Param.
* Schemas:
  * [comEperCoordSetDpaParams-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordSetDpaParams-request.json)
  * [comEperCoordSetDpaParams-respons.json](../../JsonSchemes/async-api-json-schemes/comEperCoordSetDpaParams-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.9 Set Hops

* **Message category (mcat):** comEperCoordSetHops
* Allows the specifying fixed number of hops used to send the DPA request/response or to specify an optimization algorithm to compute a number of hops.
* Schemas:
  * [comEperCoordSetHops-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordSetHops-request.json)
  * [comEperCoordSetHops-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordSetHops-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.10 Discovery Data

* **Message category (mcat):** comEperCoordDiscoveryData
* Allows reading of coordinator internal discovery data.
* Schemas:
  * [comEperCoordDiscoveryData-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordDiscoveryData-request.json)
  * [comEperCoordDiscoveryData-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordDiscoveryData-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.11 Backup

* **Message category (mcat):** comEperCoordBackup
* This command reads coordinator network information data that can be then restored to another coordinator in order to make a clone of the original coordinator.
* Schemas:
  * [comEperCoordBackup-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordBackup-request.json)
  * [comEperCoordBackup-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordBackup-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.12 Restore

* **Message category (mcat):** comEperCoordRestore
*  Allows writing previously backed up coordinator network data to the same or another coordinator device.
* Schemas:
  * [comEperCoordRestore-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordRestore-request.json)
  * [comEperCoordRestore-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordRestore-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.13 Authorize bond

* **Message category (mcat):** comEperCoordAuthBond
*  Authorizes previously remotely pre-bonded node. This assigns the node the final network address.
* Schemas:
  * [comEperCoordAuthBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordAuthBond-request.json)
  * [comEperCoordAuthBond-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordAuthBond-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.14 Read Module Id (MID)

* **Message category (mcat):** comEperCoordReadMid
* Returns module IDs and user data of the remotely pre-bonded nodes.
* Schemas:
  * [comEperCoordReadMid-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordReadMid-request.json)
  * [comEperCoordReadMid-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordReadMid-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.15 Clear Module Id (MID)

* **Message category (mcat):** comEperCoordClearMid
* Makes a node forget of the nodes that were previously remotely pre-bonded.
* Schemas:
  * [comEperCoordClearMid-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordClearMid-request.json)
  * [comEperCoordClearMid-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordClearMid-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.3.16 Enable Remote Bonding

* **Message category (mcat):** comEperCoordEnableBond
* Puts node into a mode that provides a remote bonding of up to 7 new nodes
* Schemas:
  * [comEperCoordEnableBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperCoordEnableBond-request.json)
  * [comEperCoordEnableBond-response.json](../../JsonSchemes/async-api-json-schemes/comEperCoordEnableBond-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

### 2.2.4 Embedded Periphery Node

#### 2.2.4.1 Read Parameters

* **Message category (mcat):** comEperNodeReadParams
* Reads node parameters. [Details...](https://www.iqrf.org/DpaTechGuide/#3.3%20Node)
* Schemas:
  * [comEperNodeReadParams-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeReadParams-request.json)
  * [comEperNodeReadParams-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeReadParams-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeReadParams-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeReadParams-response-example.json)

#### 2.2.4.2 Remove Bond

* **Message category (mcat):** comEperNodeRemoveBond
* Changes bonding state of node as unbonded.
* Schemas:
  * [comEperNodeRemoveBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRemoveBond-request.json)
  * [comEperNodeRemoveBond-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRemoveBond-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRemoveBond-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRemoveBond-response-example.json)


#### 2.2.4.3 Read Module Id (MID)

* **Message category (mcat):** comEperNodeReadMid
* Returns module IDs and user data of the remotely pre-bonded nodes.
* Schemas:
  * [comEperNodeReadMid-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeReadMid-request.json)
  * [comEperNodeReadMid-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeReadMid-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeReadMid-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeReadMid-response-example.json)

#### 2.2.4.4 Clear Module Id (MID)

* **Message category (mcat):** comEperNodeClearMid
* Makes a node forget of the nodes that were previously remotely pre-bonded.
* Schemas:
  * [comEperNodeClearMid-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeClearMid-request.json)
  * [comEperNodeClearMid-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeClearMid-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeClearMid-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeClearMid-response-example.json)

#### 2.2.4.5 Enable Remote Bonding

* **Message category (mcat):** comEperNodeEnableBond
* Puts node into a mode that provides a remote bonding of up to 7 new nodes
* Schemas:
  * [comEperNodeEnableBond-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeEnableBond-request.json)
  * [comEperNodeEnableBond-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeEnableBond-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeEnableBond-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeEnableBond-response-example.json)

#### 2.2.4.6 Remove bond address

* **Message category (mcat):** comEperNodeRemoveAddr
* The node stays in the IQMESH network (it is not unbonded) but a temporary address 0xFE is assigned to it.
* Schemas:
  * [comEperNodeRemoveAddr-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRemoveAddr-request.json)
  * [comEperNodeRemoveAddr-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRemoveAddr-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRemoveAddr-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRemoveAddr-response-example.json)

#### 2.2.4.7 Backup

* **Message category (mcat):** comEperNodeBackup
* TODO: Logic
* Schemas:
  * [comEperNodeBackup-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeBackup-request.json)
  * [comEperNodeBackup-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeBackup-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeBackup-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeBackup-response-example.json)

#### 2.2.4.8 Restore

* **Message category (mcat):** comEperNodeRestore
* Restore nodes with data obtained from backup command.
* Schemas:
  * [comEperNodeRestore-request.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRestore-request.json)
  * [comEperNodeRestore-response.json](../../JsonSchemes/async-api-json-schemes/comEperNodeRestore-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRestore-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperNodeRestore-response-example.json)

### 2.2.5 Embedded Periphery FRC

#### 2.2.5.1 FRC Send

* **Message category (mcat):** comFrcSend
* Fast request command to send. Parameters are FRC command and user data.
* Schemas:
  * [comFrcSend-request.json](../../JsonSchemes/async-api-json-schemes/comFrcSend-request.json)
  * [comFrcSend-responses.json](../../JsonSchemes/async-api-json-schemes/comFrcSend-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comFrcSend-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comFrcSend-response-example.json)

#### 2.2.5.2 FRC Extra Result

* **Message category (mcat):** comFrcExtraResult
* To get remaining bytes of the FRC result.
* Schemas:
  * [comFrcExtraResult-request.json](../../JsonSchemes/async-api-json-schemes/comFrcExtraResult-request.json)
  * [comFrcExtraResult-responses.json](../../JsonSchemes/async-api-json-schemes/comFrcExtraResult-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comFrcExtraResult-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comFrcExtraResult-response-example.json)


#### 2.2.5.3 FRC Send Selective

* **Message category (mcat):** comFrcSendSelective
* Fast request command for a set of nodes. Parameters are FRC command, selected nodes and user data.
* Schemas:
  * [comFrcSendSelective-request.json](../../JsonSchemes/async-api-json-schemes/comFrcSendSelective-request.json)
  * Response see DPA response [ntw-com-raw-response.json](../../JsonSchemes/async-api-json-schemes/ntw-com-raw-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comFrcSendSelective-request-example.json),


#### 2.2.5.4 FRC Set Parameters

* **Message category (mcat):** comFrcSetParams
* Set global FRC parameters.
* Schemas:
  * [comFrcSetParams-request.json](../../JsonSchemes/async-api-json-schemes/comFrcSetParams-request.json)
  * [comFrcSetParams-response.json](../../JsonSchemes/async-api-json-schemes/comFrcSetParams-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comFrcSetParams-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comFrcSetParams-response-example.json)

### 2.2.6 Embedded Periphery IO

#### 2.2.6.1 Direction

* **Message category (mcat):** comIoDir
*  Sets the direction of the individual IO pins of the individual ports. Ports are entered as an array of objects including port, mask and value. Response is status.   
* Schemas:
  * [comIoDir-request.json](../../JsonSchemes/async-api-json-schemes/comIoDir-request.json)
  * [comIoDir-responses.json](../../JsonSchemes/async-api-json-schemes/comIoDir-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comIoDir-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comIoDir-response-example.json)

#### 2.2.6.2 Set

* **Message category (mcat):** comIoSet
*  Sets the value of the individual IO pins of the individual ports. Ports are entered as an array of objects including port, mask and value. Response is status.   
* Schemas:
  * [comIoSet-request.json](../../JsonSchemes/async-api-json-schemes/comIoSet-request.json)
  * [comIoSet-responses.json](../../JsonSchemes/async-api-json-schemes/comIoSet-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comIoSet-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comIoSet-response-example.json)

#### 2.2.6.3 Get

* **Message category (mcat):** comIoGet
*  Returns the value of the individual IO pins of the individual ports. Ports are returned as an array of objects including port, mask and value.
* Schemas:
  * [comIoGet-request.json](../../JsonSchemes/async-api-json-schemes/comIoGet-request.json)
  * [comIoGet-responses.json](../../JsonSchemes/async-api-json-schemes/comIoGet-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comIoGet-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comIoGet-response-example.json)

### 2.2.7 Embedded Periphery EEPROM, EEEPROM and RAM

#### 2.2.7.1 Read

* **Message category (mcat):** comEperEepromRead, comEperEeepromRead, comEperRamRead
*  Reads data from memory.
* Schemas:
  * [comEperEepromRead-request.json](../../JsonSchemes/async-api-json-schemes/comEperEepromRead-request.json)
  * [comEperEepromRead-response.json](../../JsonSchemes/async-api-json-schemes/comEperEepromRead-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examplescomEperEepromRead-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperEepromRead-response-example.json)

#### 2.2.7.2 Write

* **Message category (mcat):** comEperEepromWrite, comEperEeepromWrite, comEperRamWrite
*  Write data to memory.
* Schemas:
  * [comEperEepromWrite-request.json](../../JsonSchemes/async-api-json-schemes/comEperEepromWrite-request.json)
  * [comEperEepromWrite-response.json](../../JsonSchemes/async-api-json-schemes/comEperEepromWrite-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperEepromWrite-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperEepromWrite-response-example.json)

### 2.2.8 Embedded Periphery UART

#### 2.2.8.1 Open

* **Message category (mcat):** comEperUartOpen
*  Opens UART peripheral at specified baud rate.
* Schemas:
  * [comEperUartOpen-request.json](../../JsonSchemes/async-api-json-schemes/comEperUartOpen-request.json)
  * [comEperUartOpen-response.json](../../JsonSchemes/async-api-json-schemes/comEperUartOpen-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperUartOpen-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperUartOpen-response-example.json)

#### 2.2.8.2 Close

* **Message category (mcat):** comEperUartClose
*  Closes UART.
* Schemas:
  * [comEperUartClose-request.json](../../JsonSchemes/async-api-json-schemes/comEperUartClose-request.json)
  * [comEperUartClose-response.json](../../JsonSchemes/async-api-json-schemes/comEperUartClose-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperUartClose-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperUartClose-response-example.json)

#### 2.2.8.3 Read

* **Message category (mcat):** comEperUartRead
*  Read data from UART. Clear flag clears RX buffer before reading.
* Schemas:
  * [comEperUartRead-request.json](../../JsonSchemes/async-api-json-schemes/comEperUartRead-request.json)
  * [comEperUartRead-response.json](../../JsonSchemes/async-api-json-schemes/comEperUartRead-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperUartRead-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperUartRead-response-example.json)

#### 2.2.8.4 Write

* **Message category (mcat):** comEperUartWrite
*  Write data to UART. Clear flag clears RX buffer before writing.
* Schemas:
  * [comEperUartWrite-request.json](../../JsonSchemes/async-api-json-schemes/comEperUartWrite-request.json)
  * [comEperUartWrite-response.json](../../JsonSchemes/async-api-json-schemes/comEperUartWrite-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperUartWrite-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperUartWrite-response-example.json)

### 2.2.9 Embedded Periphery SPI

#### 2.2.9.1 Read

* **Message category (mcat):** comEperSpiRead
*  Read data from SPI.
* Schemas:
  * [comEperSpiRead-request.json](../../JsonSchemes/async-api-json-schemes/comEperSpiRead-request.json)
  * [comEperSpiRead-response.json](../../JsonSchemes/async-api-json-schemes/comEperSpiRead-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperSpiRead-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperSpiRead-response-example.json)

#### 2.2.9.2 Write

* **Message category (mcat):** comEperSpiWrite
*  Write data to SPI.
* Schemas:
  * [comEperSpiWrite-request.json](../../JsonSchemes/async-api-json-schemes/comEperSpiWrite-request.json)
  * [comEperSpiWrite-response.json](../../JsonSchemes/async-api-json-schemes/comEperSpiWrite-response.json)
* Examples: [request](../../JsonSchemes/async-api-json-schemes/examples/comEperSpiWrite-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/comEperSpiWrite-response-example.json)

### 2.2.10 Embedded Periphery OS

#### 2.2.10.1 Read

* **Message category (mcat):** comEperOsRead
*  Returns system information of device.
* Schemas:
  * [comEperOsRead-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsRead-request.json)
  * [comEperOsRead-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsRead-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.2 Reset and Restart

* **Message category (mcat):** comEperOsReset, comEperOsRestart
*  Forces (DC)TR transceiver module to carry out reset or restart.
* Schemas:
  * [comEperOsReset-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsReset-request.json)
  * [comEperOsReset-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsReset-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.3 Read Configuration

* **Message category (mcat):** comEperOsReadCfg
*  Reads a raw HWP configuration memory.
* Schemas:
  * [comEperOsReadCfg-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsReadCfg-request.json)
  * [comEperOsReadCfg-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsReadCfg-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.4 Run RFPGM

* **Message category (mcat):** comEperOsRunRfpgm
*  Puts device into RFPGM mode.
* Schemas:
  * [comEperOsRunRfpgm-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsRunRfpgm-request.json)
  * [comEperOsRunRfpgm-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsRunRfpgm-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.5 Sleep

* **Message category (mcat):** comEperOsSleep
*  Puts the device into sleep (power saving) mode.
* Schemas:
  * [comEperOsSleep-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsSleep-request.json)
  * [comEperOsSleep-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsSleep-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.6 Run Batch

* **Message category (mcat):** comEperOsBatch
* Allows executing more individual DPA requests within one original DPA request.
* Schemas:
  * [comEperOsBatch-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsBatch-request.json)
  * [comEperOsBatch-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsBatch-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.7 Run Selective Batch

* **Message category (mcat):** comEperOsSelectiveBatch
* Allows executing more individual DPA requests within one original DPA request.
* Schemas:
  * [comEperOsSelectiveBatch-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsSelectiveBatch-request.json)
  * [comEperOsSelectiveBatch-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsSelectiveBatch-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.8 Set Security

* **Message category (mcat):** comEperOsSetSecurity
* Setting of various security parameters.
* Schemas:
  * [comEperOsSetSecurity-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsSetSecurity-request.json)
  * [comEperOsSetSecurity-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsSetSecurity-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.9 Write Configuration

* **Message category (mcat):** comEperOsWriteCfg
*  Write a raw HWP configuration memory.
* Schemas:
  * [comEperOsWriteCfg-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsWriteCfg-request.json)
  * [comEperOsWriteCfg-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsWriteCfg-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.10 Write Configuration Byte

* **Message category (mcat):** comEperOsWriteCfgByte
*  Writes multiple bytes to the HWP configuration memory.
* Schemas:
  * [comEperOsWriteCfgByte-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsWriteCfgByte-request.json)
  * [comEperOsWriteCfgByte-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsWriteCfgByte-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

#### 2.2.10.11 Load Code

* **Message category (mcat):** comEperOsLoadCode
*  Allows OTA (over the air] update of the firmware as it loads a code previously stored at external EEPROM to the MCU Flash memory.
* Schemas:
  * [comEperOsLoadCode-request.json](../../JsonSchemes/async-api-json-schemes/comEperOsLoadCode-request.json)
  * [comEperOsLoadCode-response.json](../../JsonSchemes/async-api-json-schemes/comEperOsLoadCode-response.json)
* Examples: [Generator..](http://json-schema-faker.js.org)

### 2.3 IQRF Network Management messages

### 2.3.1 Local bonding

* **Message category (mcat):** ntw-mng-bondnodelocal-request
* TODO: Logic
* Scheme:
  * [ntw-mng-bondnodelocal-request.json](../../JsonSchemes/async-api-json-schemes/ntw-mng-bondnodelocal-request.json)
  * [ntw-mng-bondnodelocal-response.json](../../JsonSchemes/async-api-json-schemes/ntw-mng-bondnodelocal-response.json)
* Example: [request](../../JsonSchemes/async-api-json-schemes/examples/ntw-mng-bondnodelocal-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/ntw-mng-bondnodelocal-request-example.json)

### 2.4 IQRF GW Daemon Scheduler messages

### 2.4.1 Schedule periodic task

* **Message category (mcat):** schedPerTask
* **Request:** Schedules periodical task which can start immediately or after defined period. Returns client id, task id and status.
* **Schemes:**
  * [schedPerTask-request.json](../../JsonSchemes/async-api-json-schemes/schedPerTask-request.json)
  * [schedPerTask-response.json](../../JsonSchemes/async-api-json-schemes/schedPerTask-response.json)
* **Examples:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedPerTask-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedPerTask-response-example.json)

### 2.4.2 Remove task

* **Message category (mcat):** schedRemTask
* **Request:** Remove task in scheduler. Task is specified by task id.
* **Scheme:**
  * [schedRemTask-request.json](../../JsonSchemes/async-api-json-schemes/schedRemTask-request.json)
  * [schedRemTask-response.json](../../JsonSchemes/async-api-json-schemes/schedRemTask-response.json)
* **Example:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedRemTask-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedRemTask-response-example.json)

### 2.4.3 List all tasks

* **Message category (mcat):** schedList
* Get list of all scheduled tasks under specific clientId.
* **Scheme:**
  * [schedList-request.json](../../JsonSchemes/async-api-json-schemes/schedList-request.json)
  * [schedList-response.json](../../JsonSchemes/async-api-json-schemes/schedList-response.json)
* **Example:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedList-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedList-response-example.json)

### 2.4.4 Remove all tasks

* **Message category (mcat):** schedRemAll
* **Request:** Remove all scheduled tasks for specific client Id. Returns client id and status.
* **Scheme:**
  * [schedRemAll-request.json](../../JsonSchemes/async-api-json-schemes/schedRemAll-request.json)
  * [schedRemAll-response.json](../../JsonSchemes/async-api-json-schemes/schedRemAll-response.json)
* **Example:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedRemAll-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedRemAll-response-example.json)

### 2.4.5 Schedule task

* **Message category (mcat):** schedAddTask
* Add new task, timing specified in cron syntax. Returns status and task id.
* **Scheme:**
  * [schedAddTask-request.json](../../JsonSchemes/async-api-json-schemes/schedAddTask-request.json)
  * [schedAddTask-response.json](../../JsonSchemes/async-api-json-schemes/schedAddTask-response.json)
* **Example:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedAddTask-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedAddTask-response-example.json)

### 2.4.6 Get task

* **Message category (mcat):** schedGetTask
* **Request:** Return task by task id.
* **Scheme:**
  * [schedGetTask-request.json](../../JsonSchemes/async-api-json-schemes/schedGetTask-request.json)
  * [schedGetTask-response.json](../../JsonSchemes/async-api-json-schemes/schedGetTask-response.json)
* **Example:** [request](../../JsonSchemes/async-api-json-schemes/examples/schedGetTask-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/schedGetTask-response-example.json)

### 2.4.7 Start task

* **Message category (mcat):** mngSchedStartTask
* **Request:** Start task specified by id. Returns task.
* **Scheme:**
  * [mngSchedStartTask-request.json](../../JsonSchemes/async-api-json-schemes/mngSchedStartTask-request.json)
  * [mngSchedStartTask-response.json](../../JsonSchemes/async-api-json-schemes/mngSchedStartTask-response.json)
* **Example:** TODO

### 2.4.8 Stop task

* **Message category (mcat):** mngSchedStopTask
* **Request:** Stop task specified by id. Returns task.
* **Scheme:**
  * [mngSchedStopTask-request.json](../../JsonSchemes/async-api-json-schemes/mngSchedStopTask-request.json)
  * [mngSchedStopTask-response.json](../../JsonSchemes/async-api-json-schemes/mngSchedStopTask-response.json)
* **Example:** TODO

### 2.5 IQRF Network Standard Device messages

### 2.5.1 Standard Device Sensor Enum

* **Message category (mcat):** ntw-com-stddev-sensor-enum
* TODO: Logic
* Schemas:
  * [ntw-com-stddev-sensor-enum-request.json](../../JsonSchemes/async-api-json-schemes/ntw-com-stddev-sensor-enum-request.json)
  * [ntw-com-stddev-sensor-enum-response.json](../../JsonSchemes/async-api-json-schemes/ntw-com-stddev-sensor-enum-response.json)
* Example: [request](../../JsonSchemes/async-api-json-schemes/examples/ntw-com-stddev-sensor-enum-request-example.json), [response](../../JsonSchemes/async-api-json-schemes/examples/ntw-com-stddev-sensor-enum-request-example.json)

### 2.6 Management Configuration messages

### 2.6.1 Gateway Restart

* **Message category (mcat):** mngConfigGtwRestart
* **Request:** Restarts gateway. Parameters is time when restart occurs.
* **Scheme:**
  * [mngConfigGtwRestart-request.json](../../JsonSchemes/async-api-json-schemes/mngConfigGtwRestart-request.json)
  * [mngSchedStopTask-response.json](../../JsonSchemes/async-api-json-schemes/mngConfigGtwRestart-response.json)
* **Example:** TODO

### 2.6.2 Gateway Operation Mode Selection

* **Message category (mcat):** mngConfigOperMode
* **Request:** Sets operation mode (operational/service/forwarding). Returns mode.
* **Scheme:**
  * [mngConfigOperMode-request.json](../../JsonSchemes/async-api-json-schemes/mngConfigOperMode-request.json)
  * [mngConfigOperMode-response.json](../../JsonSchemes/async-api-json-schemes/mngConfigOperMode-response.json)
* **Example:** TODO

### 2.6.3 Gateway Component Configuration

* **Message category (mcat):** mngConfigComponent
* **Request:** Starts, stops or reloads component. Returns command.
* **Scheme:**
  * [mngConfigComponent-request.json](../../JsonSchemes/async-api-json-schemes/mngConfigComponent-request.json)
  * [mngConfigComponent-response.json](../../JsonSchemes/async-api-json-schemes/mngConfigComponent-response.json)
* **Example:** TODO
